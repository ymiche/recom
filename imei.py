# -*- coding: utf-8 -*-
"""
IMEI data creation Module for the Recommendation System.

@author: ymiche
@version: 0.1
"""

from __future__ import with_statement  # Only for Python 2.5

import numpy as np


class IMEI(object):
    """
    New style IMEI of 15 digits as per 3GPP TS 23.003.
    Starts with 8 digits for the Type Allocation Code (TAC), replacing
    the former TAC-FAC, followed by the 6 digits serial number of the unit,
    and finally the last Check Digit (CD) computed per Luhn's algorithm.
    """

    def __init__(self, imei=None):
        """
        Generates a random IMEI if none is given.
        If given, validity is checked.
        """

        if imei is not None:
            # Check IMEI data consistency
            self.__check_imei(imei)
            if imei[-1] != self.calculate_luhn(imei):
                raise Exception('Given IMEI does not pass Luhn\'s algorithm.')
        else:
            # Generate a random one
            # Get the TAC at random from the dict of TAC
            tac = TAC.keys()[np.random.randint(1, len(TAC.keys()))]
            # Generate a random SNR unit number on 6 digits
            snr = str(np.random.randint(100000, 999999))
            # Compute the Check digit with Luhn's algorithm, one digit
            checksum = self.calculate_luhn(tac+snr)
            # Concatenate for the final imei
            imei = tac+snr+checksum
        # Affect the object
        self.imei = imei

    def __check_imei(self, imei):
        """
        Check for IMEI correctness.
        Basically type checking and Luhn's Checksum.
        Only returns exception if something is wrong.
        """
        if not isinstance(imei, str):
            raise Exception('Given IMEI is not a string.')
        if not imei.isdigit():
            raise Exception('Given IMEI does not only contain digits.')

    def luhn_checksum(self, value):
        """
        Calculates the Luhn Checksum of the value as in Luhn's algorithm
        as per U.S. Patent No. 2,950,048.
        Takes a string holding a non-zero number of digits.
        From https://en.wikipedia.org/wiki/Luhn_algorithm
        """
        # Check the structure of the given value for the checksum calculation.
        self.__check_imei(value)

        def digits_of(integerstring):
            """
            Returns the list of integers in the given string.
            """
            return [int(d) for d in str(integerstring)]

        digits = digits_of(value)
        odd_digits = digits[-1::-2]
        even_digits = digits[-2::-2]
        checksum = 0
        checksum += sum(odd_digits)
        for digit in even_digits:
            checksum += sum(digits_of(digit*2))
        return str(checksum % 10)

    def calculate_luhn(self, imei):
        """
        Calculates a check digit for imei with Luhn's algorithm as per U.S.
        Patent No. 2,950,048.
        Takes a string holding a non-zero number of digits.
        From https://en.wikipedia.org/wiki/Luhn_algorithm
        """
        # Check the structure of the given imei for the checksum calculation.
        self.__check_imei(imei)
        check_digit = self.luhn_checksum(int(imei) * 10)
        return str(check_digit) if check_digit == 0 else str(10 - check_digit)


TAC = {
    '01124500': 'Apple iPhone',
    '01130000': 'Apple iPhone model MA712LL',
    '01136400': 'Apple iPhone',
    '01154600': 'Apple iPhone model MB384LL',
    '01161200': 'Apple iPhone 3G',
    '01193400': 'Apple iPhone 3G',
    '01180800': 'Apple iPhone 3G model MB704LL',
    '01181200': 'Apple iPhone 3G model MB496B',
    '01174400': 'Apple iPhone 3G model MB496RS',
    '01194800': 'Apple iPhone 3GS',
    '01215800': 'Apple iPhone 3GS',
    '01216100': 'Apple iPhone 3GS',
    '01226800': 'Apple iPhone 3GS',
    '01215900': 'Apple iPhone 3GS model MC131B',
    '01241700': 'Apple iPhone 4',
    '01233800': 'Apple iPhone 4 model MC610LL',
    '01253600': 'Apple iPhone 4 model MC610LL/A',
    '01233700': 'Apple iPhone 4 model MC603B',
    '01233600': 'Apple iPhone 4 model MC608LL',
    '01243000': 'Apple iPhone 4 model MC603KS',
    '01254200': 'Apple iPhone 4',
    '01300600': 'Apple iPhone 4S model MD260C',
    '01332700': 'Apple iPhone 5 model MD642C',
    '01388300': 'Apple iPhone 5S model ME297C/A',
    '35876105': 'Apple iPhone 5S model A1457',
    '35450502': 'GlobeTrotter HSDPA Modem',
    '35974101': 'GlobeTrotter HSDPA Modem',
    '35896704': 'HTC Desire S',
    '35902803': 'HTC Wildfire',
    '35918804': 'HTC One X',
    '35714904': 'Huawei  e398u-15 lte stick',
    '35191405': 'Motorola Defy Mini',
    '35351200': 'Motorola V300',
    '35015100': 'Nokia 3330',
    '35089080': 'Nokia 3410 (NHM-2NX)',
    '35099480': 'Nokia 3410 (NHM-2NX)',
    '35148420': 'Nokia 3410 (NHM-2NX)',
    '35148820': 'Nokia 6310i (NPL-1)',
    '35154900': 'Nokia 6310i (NPL-1)',
    '35151304': 'Nokia E72-1 (RM-530)',
    '35274901': 'Nokia 6233',
    '35291402': 'Nokia 6210 Navigator',
    '35376800': 'Nokia 6230',
    '35566600': 'Nokia 6230',
    '35421803': 'Nokia 5310 (RM-303)',
    '35433004': 'Nokia C5-00 (RM-645)',
    '35524803': 'Nokia 2330c-2 (RM-512)',
    '35685702': 'Nokia 6300',
    '35828103': 'Nokia 6303C',
    '35693803': 'Nokia N900',
    '35694603': 'Nokia 2700',
    '35699601': 'Nokia N95',
    '35700804': 'Nokia C1',
    '35739804': 'Nokia N8',
    '35788104': 'Nokia N950',
    '35836800': 'Nokia 6230i',
    '35837800': 'Nokia N6030 (RM-74)',
    '35935003': 'Nokia 2720a-2 (RM-519)',
    '44933700': 'Nokia 6210',
    '35316605': 'Samsung GT-I9300',
    '35357800': 'Samsung SGH-A800',
    '35679404': 'Samsung Galaxy Mini (GT-S5570)',
    '35733104': 'Samsung Galaxy Gio',
    '35853704': 'Samsung Galaxy SII',
    '35226005': 'Samsung Galaxy SIII',
    '35979504': 'Samsung Galaxy Note',
    '35171005': 'Sony Ericsson Xperia S',
    '35238402': 'Sony Ericsson K770i',
    '35511405': 'Sony Ericsson Xperia U',
    '35851004': 'Sony Ericsson Xperia Active',
    '35405600': 'Wavecom M1306B',
    '35837501': 'XDA Orbit 2',
    '35316004': 'ZTE Blade',
    '35972100': 'Lobster 544',
    '35933005': 'OROD 6468'}
