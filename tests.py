# -*- coding: utf-8 -*-
"""
Tests for the Recom system

@author: ymiche
@version: 0.1
"""

from __future__ import with_statement  # Only for Python 2.5

import contextlib
import gzip

import cPickle as pkl


def main():
    """
    Main test function for now.
    """
    imeis = ['012682004524469', '354957031065660']

    for imei in imeis:
        user_states_list = []
        with contextlib.closing(gzip.GzipFile(str(imei)+'.pkl.gz', 'rb')) \
                as read_file:
            try:
                while True:
                    user_states_list.append(pkl.load(read_file))
            except EOFError:
                pass

        for myuserstate in user_states_list:
            for myuserstate2 in user_states_list:
                print myuserstate.distance(myuserstate2)

if __name__ == '__main__':
    main()
