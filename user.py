# -*- coding: utf-8 -*-
"""
User data creation Module for the Recommendation System.

@author: ymiche
@version: 0.1
"""

from __future__ import with_statement  # Only for Python 2.5

import numpy as np

from gps import GPS
from imei import IMEI
from song import GENRES, STYLES


class User(object):
    """

    Parameters
    ----------


    Attributes
    ----------
    imei            : A valid IMEI object (see the IMEI module).

    gps_dict        : A dict of valid GPS objects (see the GPS module), giving
                      the key location of the given user. If none is given,
                      the dictionary will be created with some random gps data.
                      E.g. something like:
                        {'home': GPS_object,
                         'work': GPS_object,
                         'key_0': GPS_object}

    genres_dict     : A dict with keys taken in the gps_dict keys (locations
                      names) with associated values as a list of genres, taken
                      from the list of possible genres only.
                      E.g. something like:
                        {'home': ['rock', 'pop', 'blues'],
                         'work': ['dance', 'house', 'rock'],
                         'gym' : ['house', 'techno', 'rap']}

    styles_dict     : A dict with keys taken in the gps_dict keys (locations
                      names) with associated values as a list of styles, taken
                      from the list of possible styles only.
                      Just like for genres_dict.
    """

    def __init__(self, imei=None, gps_dict=None, genres_dict=None,
                 styles_dict=None, artists_dict=None):

        # The user imei, 15 digits
        self.imei = IMEI(imei)
        # A dict holding the key locations for an user
        self.gps_dict = self.__gen_gps_dict(gps_dict)

        self.genres_dict = self.__gen_genres(genres_dict)
        self.styles_dict = self.__gen_styles(styles_dict)
        self.artists_dict = self.__gen_artists(artists_dict)

    def __check_genre(self, genre):
        """
        Checks that the given genre string is among the genres we know from the
        database.
        Only raises exception.
        """
        if not isinstance(genre, str):
            raise Exception('Given genre is not a string.')
        if genre not in GENRES:
            raise Exception('Given genre is not in the list of known genres.')

    def __gen_gps_dict(self, gps_dict=None, num_locations=2):
        """
        Generates a dict holding the key locations for the user.
        The dict is expected to have at least the 'work' and 'home' keys

        More locations are allowed with any other key name.

        If num_locations is 2, then only 'home' and 'work' are kept/generated
        If more than 2 locations are requested, keys are generated with the
        namestyle 'key_i', with i an integer starting at 0.
        """
        # If we get an empty dict, create a random one, with the default data
        if gps_dict is None:
            gps_dict = {'home': GPS(),
                        'work': gps_dict['home'].move_around_gps(radius=5000,
                                                       movement_quant='high',
                                                       num_elem=1)[0]}
        # Check that the given dict has the required home and work keys
        if not isinstance(gps_dict, dict):
            raise Exception('Given gps dict is not a dict.')
        if ('home' not in gps_dict) or ('work' not in gps_dict):
            raise Exception('Given gps dict does not have the home \
                                and work keys.')
        for i in xrange(num_locations-2):
            new_location = 'key_'+str(i)
            gps_dict[new_location] = gps_dict['home'].move_around_gps(
                                                        radius=20000,
                                                        movement_quant='high',
                                                        num_elem=1)[0]
        self.gps_dict = gps_dict

    def __gen_genres(self, genres_dict=None):
        """
        Generates a dictionary with keys as the gps dict keys, and lists of
        genres associated to the location.
        The given genres in the dict are checked against a valid list of
        genres.
        """
        # Check the given genres in the lists against the list of valid genres
        if genres_dict is not None:
            if 'home' not in genres_dict:
                raise Exception('Home key is missing from the dict of genres.')
            if 'work' not in genres_dict:
                raise Exception('Work key is missing from the dict of genres.')
            for key in genres_dict:
                if not isinstance(key, str):
                    raise Exception('Key from the dict of genres is not a \
                                     string.')
                for genre in genres_dict[key]:
                    self.__check_genre(genre)
        else:
            # We need to generate a default dict with the right keys: only
            # 'home' and 'work'.
            genres_dict = {'home': self.__gen_similar_genres(number_elements=3),
                           'work': self.__gen_similar_genres(number_elements=3)}

        self.genres_dict = genres_dict

    def __gen_similar_genres(self, genres_list=None, number_elements=1):
        """
        Generates a list of elements taken from GENRES keys, such that the
        elements are not far from each other in the SensMe sense.
        If a genre or a list of genres is given in argument, it generates
        elements that are close to the given ones.
        The function generates number_elements *additional* elements and
        returns them as a list.
        """
        # Check the given arguments
        if genres_list is not None:
            for genre in genres_list:
                self.__check_genre(genre)
        else:
            # We generate a list with only one element taken at random from the
            # list of genres
            genres_list = GENRES.keys()[np.random.randint(1, len(GENRES))]

        # The list holding the new genres
        new_genres_list = []

        for i in xrange(number_elements):
            # Take an element at random from the given list of genres, if any
            genre = genres_list[np.random.randint(1, len(genres_list))]
            # Get the ten closest genres in terms of distance, sorted
            closest_elements = self.get_closest_elements(genre, GENRES,
                                                         number_elements=10)
            # Take one element at random in the list of ten and append
            new_genres_list.append(closest_elements[np.random.randint(1,
                                                    len(closest_elements))])
        return new_genres_list

    def get_closest_elements(self, dict_key, ref_dict, number_elements=1):
        """
        Calculates the distances between the given element and all the other
        elements in ref_dict, and returns the number_elements closest elements
        in a list, sorted by distance.
        """
        if not isinstance(dict_key, str):
            raise Exception('Given dict key is not a string.')
        if not isinstance(ref_dict, dict):
            raise Exception('Given reference dict is not a dict.')
        if dict_key not in ref_dict:
            raise Exception('Given dict key is not in the reference dict.')
        if not isinstance(number_elements, int):
            raise Exception('Given number of elements is not an integer.')
        if number_elements <= 0:
            raise Exception('Given number of elements is not strictly \
                                                                positive')
        if number_elements >= (len(ref_dict) - 1):
            raise Exception('Given number of elements is too high.')

        # TODO : Fix this Ugly formatting...
        sorted_elements = sorted([(np.linalg.norm(np.array(ref_dict[dict_key]) - np.array(ref_dict[key])), key) for key in ref_dict.keys()])

        # TODO : Fix this Ugly formatting...
        closest_elements = [key for (key, dist) in sorted_elements][1:number_elements]
        return closest_elements

    def __gen_styles(self, styles_dict=None):
        """
        Generates a dictionary with keys as the gps dict keys, and lists of
        styles associated to the location.
        The given styles in the dict are checked against a valid list of
        styles.
        """
        # Check the given styles in the lists against the list of valid styles
        if styles_dict is not None:
            if 'home' not in styles_dict:
                raise Exception('Home key is missing from the dict of styles.')
            if 'work' not in styles_dict:
                raise Exception('Work key is missing from the dict of styles.')
            for key in styles_dict:
                if not isinstance(key, str):
                    raise Exception('Key from the dict of styles is not a \
                                     string.')
                for style in styles_dict[key]:
                    self.__check_style(style)
        else:
            # We need to generate a default dict with the right keys: only
            # 'home' and 'work'.
            styles_dict = {'home': self.__gen_similar_styles(number_elements=3),
                           'work': self.__gen_similar_styles(number_elements=3)}

        self.styles_dict = styles_dict

    def __gen_similar_styles(self, styles_list=None, number_elements=1):
        """
        Generates a list of elements taken from STYLES keys, such that the
        elements are not far from each other in the SensMe sense.
        If a style or a list of styles is given in argument, it generates
        elements that are close to the given ones.
        The function generates number_elements *additional* elements and
        returns them as a list.
        """
        # Check the given arguments
        if styles_list is not None:
            for style in styles_list:
                self.__check_style(style)
        else:
            # We generate a list with only one element taken at random from the
            # list of style
            styles_list = STYLES.keys()[np.random.randint(1, len(STYLES))]

        # The list holding the new styles
        new_styles_list = []

        for i in xrange(number_elements):
            # Take an element at random from the given list of styles, if any
            style = styles_list[np.random.randint(1, len(styles_list))]
            # Get the ten closest styles in terms of distance, sorted
            closest_elements = self.get_closest_elements(style, STYLES,
                                                         number_elements=10)
            # Take one element at random in the list of ten and append
            new_styles_list.append(closest_elements[np.random.randint(1,
                                                    len(closest_elements))])
        return new_styles_list

    def __check_style(self, style):
        """
        Checks that the given style string is among the styles we know from the
        database.
        Only raises exception.
        """
        if not isinstance(style, str):
            raise Exception('Given style is not a string.')
        if style not in STYLES:
            raise Exception('Given style is not in the list of known styles.')

    def __gen_artists(self, artists_dict=None):
        """
        Generates a dictionary with keys as the gps dict keys, and lists of
        artists associated to the location.
        The given artists in the dict are checked against the Discogs database
        to check for existence.

        If no artists are given, a dict of artists is created based on the
        existing styles and genres dict.
        """
        # Check the given artists in the lists against the Discogs database
        if artists_dict is not None:
            if 'home' not in artists_dict:
                raise Exception('Home key is missing from the dict of \
                                artists.')
            if 'work' not in artists_dict:
                raise Exception('Work key is missing from the dict of \
                                artists.')
            for key in artists_dict:
                if not isinstance(key, str):
                    raise Exception('Key from the dict of artists is not a \
                                     string.')
                for artist in artists_dict[key]:
                    self.__check_artist(artist)

        else:
            # Create the basic dict of home and work keys with the artists
            # chosen at random based on the genres and styles available for
            # each location
            artists_dict = {'home': ,
                            'work': }

        self.artists_dict = artists_dict

    def __check_artist(self, artist):
        """
        Checks against the database for the existence of an artist.
        Only raises Exception if there is a problem.
        """
        if not isinstance(artist, str):
            raise Exception('Given artist is not a string.')
        # FIXME : Put the actual verification against the Discogs database here

    def __gen_random_artist_from_genres(self, genres_list, number_elements=1):
        """
        Generates a list of number_elements artist names taken at random from
        all available artists in the database based on the genres available in
        the genres_list.

        It favours those artists that fit several genres that are in the
        genres_list.
        """
        # Check the arguments
        if not isinstance(genres_list, list):
            raise Exception('Given genres list is not a list')
        for genre in genres_list:
            if genre not in GENRES:
                raise Exception('Given genres list contains unknown genres.')
        if not isinstance(number_elements, int):
            raise Exception('Given number of elements is not an integer.')
        if number_elements <= 0:
            raise Exception('Given number of elements is not strictly \
                            positive.')

        artists_list = []
        return artists_list
