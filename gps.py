# -*- coding: utf-8 -*-
"""
GPS data creation Module for the Recommendation System.

@author: ymiche
@version: 0.1
"""

from __future__ import with_statement  # Only for Python 2.5

import numpy as np
from geopy import geocoders
from geopy import distance


# TODO : Add more cities
CITIES_GPS = {'Helsinki': (60.1733244, 24.9410248),
              'Tampere': (61.4981508, 23.7610254),
              'Turku': (60.4326518, 22.2253995),
              'Oulu': (65.0126148, 25.4714526)}


class GPS(object):
    """
    GPS object holding a tuple of two floats representing the GPS coordinates
    of a location.

    Following specifications from WGS 1984, EPSG:4326,
    http://spatialreference.org/ref/epsg/wgs-84/

    Latitude values are expected as floats, within -180 and 180.
    Longitude values are expected as floats, within -90 and 90.

    Uses GoogleMap APIv3 for Geocoding and Reverse Geocoding.
    https://developers.google.com/maps/documentation/javascript/
    """

    def __init__(self, gps=None):
        """
        If data is given as a tuple, check its consistency and apply it to the
        object.
        """
        if gps is not None:
            # Check gps data consistency, if given
            self.__chk_gps_tuple(gps)

        result = self.__gen_rand_address_gps(gps)

        # The exact latitude and longitude given, if any
        self.latitude = gps[0]
        self.longitude = gps[1]
        self.addr_latitude = result[0]
        self.addr_longitude = result[1]
        self.text_address = result[2]

    def address_coordinates(self):
        """
        Returns the reverse geocoded address gps coordinates as a tuple of
        floats.
        """
        return (self.addr_latitude, self.addr_longitude)

    def exact_coordinates(self):
        """
        Returns the exact given gps coordinates as a tuple of floats.
        """
        return (self.latitude, self.longitude)

    def __chk_gps_tuple(self, gps):
        """
        Check the gps data for consistency.
        Does nothing but raise an exception if something is wrong.
        """
        # Check that the given gps is a couple of values as floats
        if not isinstance(gps, tuple):
            raise Exception('Given gps coordinates is not a tuple.')
        if not len(gps) == 2:
            raise Exception('Given gps coordinates are not a couple.')
        if (not isinstance(gps[0], float)) or \
                (not isinstance(gps[1], float)):
            raise Exception('Given gps coordinates are not floats.')

    def __gen_rand_address_gps(self, gps=None):
        """
        Generate a random gps location that is a livable address.
        If the given gps coordinates are None, then a random place is
        generated. Otherwise, the closest address is used and its gps
        coordinates are returned.
        """
        if gps is not None:
            # Check gps data consistency, if given
            self.__chk_gps_tuple(gps)
        else:
            # No gps data given, generate random one, close to a major living
            # center
            gps = CITIES_GPS[CITIES_GPS.keys()[np.random.randint(1,
                                                            len(CITIES_GPS))]]
            # Generate a bit of movement around, about 5km radius
            gps = self.move_around_gps(radius=5000,
                                       movement_quant='high',
                                       num_elem=1)[0]
        # Finally reverse geocode the gps to an existing nearby address
        geocoder = geocoders.GoogleV3()
        results = geocoder.reverse(gps)
        # And use the found address gps coordinates as result
        return results[0].latitude, results[0].longitude, results[0].address

    def is_within_range(self, gps, max_range):
        """
        Checks whether the given gps coordinates are within max_range
        meters of the objects coordinates.
        Range is expected in meters, integer only.

        Uses the addr_latitude/longitude of the reverse geocoded address, not
        the exact coordinates given of the gps objects, if any.
        """
        # Check gps data structure
        if not isinstance(gps, GPS):
            raise Exception('Given gps argument is not a GPS object.')
        # Check that the radius is a reasonable number
        if not isinstance(max_range, int):
            raise Exception('Given max_range is not an integer.')
        if max_range <= 0:
            raise Exception('Given max_range is not a positive integer.')

        if distance.distance(self.address_coordinates(),
                             gps.address_coordinates()) <= float(max_range):
            return True
        else:
            return False

    def move_around_gps(self, radius=10, movement_quant='small', num_elem=1):
        """
        Returns a list of gps objects within radius meters of the original
        one, with a quantity of movement quantified by movement_quant.
        Uses the ideas from http://gis.stackexchange.com/questions/25877/how-to
        -generate-random-locations-nearby-my-location
        """

        # Check that the radius is a reasonable number
        if not isinstance(radius, int):
            raise Exception('Given radius is not an integer.')
        if radius <= 0:
            raise Exception('Given radius is not a positive integer.')

        # Check that the movement quantity is correct
        if movement_quant not in ['small', 'medium', 'large']:
            raise Exception('Given movement quantity not in small, medium \
                            or large.')

        # Check that the number of elements requested is correct
        if not isinstance(num_elem, int):
            raise Exception('Given number of elements is not an integer.')
        if num_elem <= 0:
            raise Exception('Given number of elements is not a positive \
                integer.')

        gps_movements = []

        # Simple quantification of the quantity of movement to generate
        # Small quantiy means locations will not be more than 3 meters from
        # each other, mostly for walking type movement
        if movement_quant == 'small':
            max_distance = 3
        # Medium gives 20 meters away, for bike or fast running
        elif movement_quant == 'medium':
            max_distance = 20
        else:
            # High is for the car/fast transportation means, 100 meters
            max_distance = 100

        for i in xrange(num_elem):
            # If this is the first update to the gps position, take the given
            # original position as the origin
            if i == 0:
                new_gps = self.gen_rand_nearby_gps(max_distance)
                while not self.is_within_range(new_gps, radius):
                    new_gps = self.gen_rand_nearby_gps(max_distance)
                gps_movements.append(new_gps)
            # Otherwise, we want to have the next position related to the
            # previous one
            else:
                new_gps = self.gen_rand_nearby_gps(max_distance,
                                                   gps=gps_movements[-1])
                # And it has to be within the given range of the original
                # location
                while not self.is_within_range(new_gps, radius):
                    new_gps = self.gen_rand_nearby_gps(gps_movements[-1],
                                                       max_distance)
                gps_movements.append(new_gps)
        return gps_movements

    def gen_rand_nearby_gps(self, max_dist, gps=None):
        """
        Generate a random gps location nearby the given one, within the
        given distance.
        """
        # If a specific gps object is given, we generate a new location
        # starting from this location, and not from the self
        if gps is not None:
            if not isinstance(gps, GPS):
                raise Exception('Given gps is not a GPS object.')
            coordinates = gps.address_coordinates()
        else:
            # We do it from the current object
            coordinates = self.address_coordinates()

        # Check that the given max_dist is a positive integer
        if not isinstance(max_dist, int):
            raise Exception('Given max_dist is not an integer.')
        if max_dist <= 0:
            raise Exception('Given max_dist is not positive.')

        # Draw random numbers for the new location
        num1 = np.random.random()
        num2 = np.random.random()
        # There are about 111,300 meters in a degree at the Equator
        radius_in_degrees = float(max_dist)/111300.0
        num3 = radius_in_degrees * np.sqrt(num1)
        num4 = 2 * np.pi * num2
        delta_x = num3 * np.cos(num4)
        delta_y = num3 * np.sin(num4)
        # Adjust the x-coordinate for the shrinking of the east-west
        # distances
        delta_x = delta_x/float(coordinates[1])
        new_latitude = coordinates[0]+delta_x
        new_longitude = coordinates[1]+delta_y
        new_gps = GPS(gps=(new_latitude, new_longitude))
        return new_gps
